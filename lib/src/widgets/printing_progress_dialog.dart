// ignore_for_file: use_build_context_synchronously

import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';

import '../../flutter_bluetooth_printer_library.dart';

class PrintingProgressDialog extends StatefulWidget {
  final String device;
  final ReceiptController controller;
  final Function()? onDone;
  final Function()? onError;

  const PrintingProgressDialog({
    Key? key,
    required this.device,
    required this.controller,
    this.onDone,
    this.onError,
  }) : super(key: key);

  @override
  State<PrintingProgressDialog> createState() => _PrintingProgressDialogState();
  static void print(
    BuildContext context, {
    required String device,
    required ReceiptController controller,
    final Function()? onDone,
    final Function()? onError,
  }) async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => PrintingProgressDialog(
        controller: controller,
        device: device,
        onDone: onDone,
        onError: onError,
      ),
    );
  }
}

class _PrintingProgressDialogState extends State<PrintingProgressDialog> {
  double? progress;
  int percent = 0;
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      try {
        await widget.controller.print(
          address: widget.device,
          linesAfter: 2,
          useImageRaster: true,
          keepConnected: Platform.isAndroid,
          onProgress: (total, sent) {
            if (mounted) {
              setState(() {
                progress = sent / total;
              });
              if (progress == 1) {
                widget.onDone?.call();
              }
            }
          },
        );
      } catch (e) {
        widget.onError?.call();
      }
    });

    Timer? timer;
    timer = Timer.periodic(const Duration(milliseconds: 140), (_) {
      if (mounted) {
        setState(() {
          percent += 1;
          if (percent >= 100) {
            timer?.cancel();
            // percent=0;
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Dialog(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const Text(
                'Printing Receipt',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 8),
              LinearProgressIndicator(
                value: progress,
                backgroundColor: Colors.grey.shade200,
              ),
              const SizedBox(height: 4),
              if (Platform.isAndroid)
                Text('Processing: $percent%')
              else
                Text('Processing: ${((progress ?? 0) * 100).round()}%'),
              // if (((progress ?? 0) * 100).round() == 100) ...[
              //   const SizedBox(height: 16),
              //   ElevatedButton(
              //     onPressed: () async {
              //       await FlutterBluetoothPrinter.disconnect(widget.device);
              //       Navigator.pop(context);
              //     },
              //     child: const Text('Close'),
              //   )
              // ]
            ],
          ),
        ),
      ),
    );
  }
}
