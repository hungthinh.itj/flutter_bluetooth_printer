part of flutter_bluetooth_printer;

class BluetoothDeviceSelector extends StatefulWidget {
  final Widget? disabledWidget;
  final Widget? permissionRestrictedWidget;
  final Widget? title;

  const BluetoothDeviceSelector({
    Key? key,
    this.disabledWidget,
    this.permissionRestrictedWidget,
    this.title,
  }) : super(key: key);

  @override
  State<BluetoothDeviceSelector> createState() =>
      _BluetoothDeviceSelectorState();
}

class _BluetoothDeviceSelectorState extends State<BluetoothDeviceSelector> {
  List<scan.BluetoothDevice> devices = [];
  late scan.FlutterScanBluetooth _bluetooth;

  @override
  void initState() {
    super.initState();
    _bluetooth = scan.FlutterScanBluetooth();
    _bluetooth.devices.listen((device) {
      if (!devices.contains(device)) {
        devices.add(device);
      }
      if (mounted) {
        setState(() {});
      }
    });
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      _bluetooth.startScan(pairedDevices: true);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: widget.title ??
                  const Text(
                    'Chọn thiết bị',
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
            ),
            IconButton(
                onPressed: () async {
                  devices.clear();
                  await _bluetooth.startScan(pairedDevices: true);
                },
                icon: const Icon(Icons.refresh_rounded))
          ],
        ),
        Expanded(
          child: StreamBuilder<DiscoveryState>(
            stream: FlutterBluetoothPrinter.discovery,
            builder: (context, snapshot) {
              // if (snapshot.connectionState == ConnectionState.waiting) {
              //   return const Center(child: CircularProgressIndicator());
              // }

              final data = snapshot.data;

              if (data is BluetoothDisabledState) {
                return widget.disabledWidget ??
                    const Center(
                      child: Text('Bluetooth đã tắt'),
                    );
              }

              if (data is PermissionRestrictedState) {
                return widget.permissionRestrictedWidget ??
                    const Center(
                      child: Text('Bluetooth chưa được cấp quyền sử dụng'),
                    );
              }

              if (data is BluetoothEnabledState) {
                return const Center(child: CircularProgressIndicator());
              }
              final nearByDevices = devices
                  .where((e) => e.nearby && !e.name.contains(e.address))
                  .toList();
              final pairedDevices = devices.where((e) => e.paired).toList();
              final allDevices = pairedDevices + nearByDevices;
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: (allDevices.isNotEmpty)
                    ? ListView.builder(
                        itemCount: allDevices.length,
                        itemBuilder: (context, index) {
                          final device = allDevices.elementAt(index);

                          return ListTile(
                            title: Text(device.name),
                            subtitle: Text(device.address),
                            leading: const Icon(Icons.bluetooth),
                            onTap: () async {
                              Navigator.pop(context, device);
                            },
                          );
                        },
                      )
                    : const Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Center(child: Text('Không có thiết bị')),
                      ),
              );
            },
          ),
        ),
      ],
    );
  }
}
